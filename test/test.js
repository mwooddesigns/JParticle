var c = document.getElementById('test'),
    ctx = c.getContext('2d');

var HEIGHT = window.innerHeight,
    WIDTH = window.innerWidth;

c.width = WIDTH;
c.height = HEIGHT;

var pos = new JVector(WIDTH/2, HEIGHT/2);
var vel = new JVector(1, 5);
var acc = new JVector(0, -0.1);
var test = new JParticle(pos, vel, acc, 5);

function render() {
  ctx.fillStyle = "#000";
  ctx.fillRect(0, 0, WIDTH, HEIGHT);
  test.draw(ctx);
  test.update();
  requestAnimationFrame(render);
}

render();
