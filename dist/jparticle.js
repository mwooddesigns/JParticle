"use strict";

function JVector() {
    this.x = arguments[0], this.y = arguments[1], arguments.length >= 3 && (this.z = arguments[2]), 
    this.size = arguments.length;
}

JVector.prototype.print = function() {
    var t;
    switch (this.size) {
      case 2:
        t = "{x: " + this.x + ", y: " + this.y + "}";
        break;

      case 3:
        t = "{x: " + this.x + ", y: " + this.y + ", z: " + this.z + "}";
    }
    console.log(t);
}, JVector.prototype.random2D = function() {
    this.x = 2 * (Math.random() - .5), this.y = 2 * (Math.random() - .5), this.z = null, 
    this.size = 2, this.normalize();
}, JVector.prototype.random3D = function() {
    this.x = 2 * (Math.random() - .5), this.y = 2 * (Math.random() - .5), this.z = 2 * (Math.random() - .5), 
    this.size = 3, this.normalize();
}, JVector.prototype.set = function() {
    this.x = arguments[0], this.y = arguments[1], 2 == arguments.length && (this.size = 2, 
    this.z = 0), arguments.length >= 3 && (this.size = 3, this.z = arguments[2]);
}, JVector.prototype.copy = function() {
    return this;
}, JVector.prototype.add = function(t) {
    this.x += t.x, this.y += t.y, this.size >= 3 && t.size >= 3 && (this.z += t.z);
}, JVector.prototype.sub = function(t) {
    this.x -= t.x, this.y -= t.y, this.size >= 3 && t.size >= 3 && (this.z -= t.z);
}, JVector.prototype.mult = function(t) {
    this.x *= t, this.y *= t, this.size >= 3 && (this.z *= t);
}, JVector.prototype.div = function(t) {
    this.x /= t, this.y /= t, this.size >= 3 && (this.z /= t);
}, JVector.prototype.dist = function(t) {
    if (this.size != t.size) return void console.log("Error: Vector Size Mismatch");
    var i = Math.pow(this.x - t.x, 2) + Math.pow(this.y - t.y, 2);
    console.log(i);
}, JVector.prototype.mag = function() {
    var t;
    switch (this.size) {
      case 2:
        t = Math.sqrt(this.x * this.x + this.y * this.y);
        break;

      case 3:
        t = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }
    return t;
}, JVector.prototype.magSq = function() {
    return this.mag() * this.mag();
}, JVector.prototype.normalize = function() {
    var t = this.mag();
    this.div(t);
};

"use strict";

function JParticle() {
    this.argLength = arguments.length;
    this.pos = arguments[0];
    this.vel = arguments[1];
    this.acc = arguments[2];
    this.size = arguments[3];
}

JParticle.prototype.print = function() {
    var output;
    output = "Position: " + this.pos.x + ", " + this.pos.y + "\r\n";
    output += "Velocity: " + this.vel.x + ", " + this.vel.y + "\r\n";
    output += "Acceleration: " + this.acc.x + ", " + this.acc.y + "\r\n";
    output += "Size: " + this.size;
    console.log(output);
};

JParticle.prototype.update = function() {
    this.pos.add(this.vel);
    this.vel.add(this.acc);
};

JParticle.prototype.draw = function(ctx) {
    ctx.fillStyle = "#fff";
    ctx.beginPath();
    ctx.arc(this.pos.x, this.pos.y, this.size, 0, Math.PI * 2);
    ctx.closePath();
    ctx.fill();
};