"use strict";

function JParticle() {
  this.argLength = arguments.length;
  this.pos = arguments[0];
  this.vel = arguments[1];
  this.acc = arguments[2];
  this.size = arguments[3]
}

JParticle.prototype.print = function() {
  var output;
  output = "Position: " + this.pos.x + ", " + this.pos.y + "\r\n";
  output += "Velocity: " + this.vel.x + ", " + this.vel.y + "\r\n";
  output += "Acceleration: " + this.acc.x + ", " + this.acc.y + "\r\n";
  output += "Size: " + this.size;
  console.log(output);
}

JParticle.prototype.update = function() {
  this.pos.add(this.vel);
  this.vel.add(this.acc);
}

JParticle.prototype.draw = function(ctx) {
  ctx.fillStyle = "#fff";
  ctx.beginPath();
  ctx.arc(this.pos.x, this.pos.y, this.size, 0, Math.PI * 2);
  ctx.closePath();
  ctx.fill();
}
